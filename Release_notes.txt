Release Notes:20-April-2018

Addressed issue of child package printing in MPS flow.


Release Notes:17-April-2018

Addressed issue with UNID , in back & forward navigation scenario�s.


Release Notes:13-April-2018

Hummresults
Nil

Hummrequests:
Nil

Fixed the issue of copying  routing data of master to child (w.r.t to  Country,Location,Postal and state)


Release Notes:12-April-2018

Hummresults

DGNotAllowed is removed .

Added:
DGServiceNotAllowed : Returned when class 7.0 is selected for FO service.
DGDoNotPickUp:returned when class 1.3 is selected.
DGClassNotAllowedForService : returned when entered UNID is not vlaid for selected service.
InvalidBSODestCorrectorContinue: returned when entered BSO destination postal is invalid.

ExtraChargeNotAllowedWithODA,
ExtraChargeNotAllowedWithOPA,
ODANotAllowedWithOtherExtraChargeOptions,
OPANotAllowedWithOtherExtraChargeOptions,
SelectUpto4ExtraChargeOptions,
InvalidStorageDays,

Hummrequests:
NumberOfStorageDays,

New/Modified existing functaionality :
Created entity classes for Booking Number and Shipper Reference Number

Added new business rule for BSO Destination validation.
Addressed issue with DG conversion.



Release Notes:02-April-2018

1.)Fixed issue with IsSaturdayDelivery.
2.)Addressed Dangerous Goods special handling conversions as per given UNID.


Release Notes:29-March-2018

Newly added in current version:

1)New Hummrequests and Results 


Hummresults

CONSNotInitialized,
HalfAstraNotAllowedForREX,
InvalidAirportId,
InvalidAirportIdLength,
InvalidApproverName,
InvalidApproverNameLength,
InvalidShipperReferenceNumber,
SelectPAOrPAPlusSplHandling





Hummrequets

AirportId
BSODestinationPostal,
BSOPostalCityState,
BSOPostalCity,
BSODestinationCity,
BSOPostal,
BSODestinationState,
BSORoutingCode,
FirstInitialAndLastName,
OpenCONS,
PrintAndApplyOverExistingLabel,
PrintFreightReceipt


Validation of scanned weight barcode with interleaved symbology for Rex- reweigh



************************************************************************************************


Release Notes:13-March-2018.

Newly added in current version:
1) PUX26
2) POD support for TNT.
3) CONS:
	DELCONS(Except Report.)
	


All below existing PUP,PUX,REX are baselined with UI state Machine required Result/Request.

PUP/PUX: USPS Pickup,
	PUX-3,PUX-05,PUX-08,PUX-13,PUX-15,PUX-16,PUX-17,PUX-20,PUX-23,PUX-24,PUX-30,PUX-35,PUX-39,PUX-40,PUX-43,
	PUX-46,PUX-47,PUX-50,PUX-78,PUX-81,PUX-84,PUX-91,PUX-92,PUX-93,PUX-94,PUX-95,PUX-96,PUX-98


REX:
	REX82:REX_DIM_WEIGHT
	REX83:REX_PHONE,REX_CITY_STATE,REX_REWEIGH,REX_PAYMENTMETHOD,REX_WEIGHT,REX_PIECES,REX_DUPLICATELABEL
	      REX_COLD STORAGE,REX_REICE,REX_GELPAK,REX_CHARGE,REX_NON_STACKABLE.
	      (Routed):REX_CITY_COUNTRY,REX_SERVICE_CHANGE,REX_NON_CONVEYABLE,REX_ZIP_POSTALCODE,REX_STREET_ADDRESS



